package main

import (
	"context"
	"errors"
	"io"
	"log"
	"time"

	bds "gitlab.com/ejv-demo-go/go-grpc-walkthrough/bidirectstream/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func main() {
	cc, err := grpc.Dial("localhost:50058", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	defer func(cc *grpc.ClientConn) {
		err = cc.Close()
		if err != nil {
			log.Println(err)
		}
	}(cc)

	client := bds.NewRaceCommsClient(cc)

	log.Println("Starting to do a Bidirectional Stream Phone RPC...")

	BidirectionalStreamSendBytesMsg(client)
	BidirectionalStreamSendStrMsg(client)
}

func BidirectionalStreamSendBytesMsg(client bds.RaceCommsClient) {
	requests := []*bds.SendMsgBytesRequest{
		{
			Msg: []byte("Safety Car window is open, just info!"),
		},
		{
			Msg: []byte("Let me know track condition"),
		},
		{
			Msg: []byte("Gap?"),
		},
		{
			Msg: []byte("Brake Fade"),
		},
		{
			Msg: []byte("end"),
		},
	}

	respStream, err := client.SendMsgBytes(context.Background())
	if err != nil {
		log.Fatalf("error while calling Bidirectional Streaming: %v", err)
	}

	for _, req := range requests {
		log.Printf("Sending bytes req: %v\n", req)
		if err = respStream.Send(req); err != nil {
			return
		}
		time.Sleep(1 * time.Second)
	}

	err = respStream.CloseSend()
	if err != nil {
		log.Fatalf("error while receiving response from Bidirectional Streaming: %v", err)
	}

	var responses []*bds.SendMsgBytesResponse
	for {
		resp, err := respStream.Recv()
		if errors.Is(err, io.EOF) {
			break
		}
		responses = append(responses, resp)
		time.Sleep(1 * time.Second)
		log.Println("Bytes SendMessageResponse:", resp)
	}
	log.Println("All bytes SendMessageResponse", responses)
}

func BidirectionalStreamSendStrMsg(client bds.RaceCommsClient) {
	requests := []*bds.SendMsgStrRequest{
		{
			Msg: "Box this lap, box, pit confirm",
		},
		{
			Msg: "What's the gap?",
		},
		{
			Msg: "Hamilton has pitted for inters, for reference, and Perez is three seconds behind. Push, push",
		},
		{
			Msg: "Charles is coming in for inters. Verstappen 2.8 behind, 2.8.",
		},
		{
			Msg: "Ok, let's hold to see what will happen. Have a good one!",
		},
	}

	respStream, err := client.SendMsgStr(context.Background())
	if err != nil {
		log.Fatalf("error while calling Bidirectional Streaming: %v", err)
	}

	var responses []*bds.SendMsgStrResponse
	for _, req := range requests {
		log.Printf("Sending str req: %v\n", req)
		if err = respStream.Send(req); err != nil {
			return
		}
		time.Sleep(1 * time.Second)
		resp, err := respStream.Recv()
		if errors.Is(err, io.EOF) {
			break
		}
		log.Println("Str SendMessageResponse:", resp)
		time.Sleep(1 * time.Second)
		responses = append(responses, resp)
	}
	log.Println("All str SendMessageResponse", responses)

	err = respStream.CloseSend()
	if err != nil {
		log.Fatalf("error while receiving response from Bidirectional Streaming: %v", err)
	}
}
