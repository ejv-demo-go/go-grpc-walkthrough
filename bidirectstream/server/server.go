package main

import (
	"errors"
	"io"
	"log"
	"net"
	"time"

	bds "gitlab.com/ejv-demo-go/go-grpc-walkthrough/bidirectstream/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

type raceCommsServer struct {
	bds.RaceCommsServer
	calls []string
}

func main() {
	listener, err := net.Listen("tcp", "0.0.0.0:50058")
	if err != nil {
		_ = errors.New("failed to listen: the port")
	}
	log.Print("Server started")
	s := grpc.NewServer()
	bds.RegisterRaceCommsServer(s, &raceCommsServer{})

	// Register reflection service on gRPC server.
	reflection.Register(s)

	if err = s.Serve(listener); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}

func (r *raceCommsServer) SendMsgBytes(stream bds.RaceComms_SendMsgBytesServer) (err error) {
	log.Println("Server SendMsgBytes started")
	for {
		req, err := stream.Recv()
		if err != nil {
			// io.EOF is used to check the end of the stream.
			// Checking for the client stream message in the infinite loop and sending the response messages,
			// once the client ends the stream then we break the loop.
			if errors.Is(err, io.EOF) {
				return nil
			}
			return err
		}

		r.calls = append(r.calls, string(req.Msg))
		log.Println("record Bytes calls received number is ", len(r.calls))

		if r.calls[len(r.calls)-1] == "end" {
			for _, msg := range r.calls {
				time.Sleep(time.Second)
				switch msg {
				case "end":
					_ = stream.Send(&bds.SendMsgBytesResponse{
						Msg: []byte("That's P1!, Great Driving!"),
					})
				case "Safety Car window is open, just info!":
					_ = stream.Send(&bds.SendMsgBytesResponse{
						Msg: []byte("copy!"),
					})
				case "Let me know track condition":
					_ = stream.Send(&bds.SendMsgBytesResponse{
						Msg: []byte("Drying up quickly"),
					})
				case "Gap?":
					_ = stream.Send(&bds.SendMsgBytesResponse{
						Msg: []byte("Gap to Sainz behind, Sainz lap time 32.6"),
					})
				default:
					_ = stream.Send(&bds.SendMsgBytesResponse{
						Msg: []byte("Box! Box! Box!!"),
					})
				}
			}
			r.calls = r.calls[:0]
		}
	}
}

func (r *raceCommsServer) SendMsgStr(stream bds.RaceComms_SendMsgStrServer) (err error) {
	log.Println("Server SendMsgStr started")
	for {
		req, err := stream.Recv()
		if err != nil {
			// io.EOF is used to check the end of the stream.
			// Checking for the client stream message in the infinite loop and sending the response messages,
			// once the client ends the stream then we break the loop.
			if errors.Is(err, io.EOF) {
				return nil
			}
			return err
		}

		r.calls = append(r.calls, req.Msg)
		log.Println("record Str calls received number is ", len(r.calls))

		for _, msg := range r.calls {
			time.Sleep(time.Second)
			switch msg {
			case "Box this lap, box, pit confirm":
				_ = stream.Send(&bds.SendMsgStrResponse{
					Msg: "Confirm, Box this lap",
				})
			case "What's the gap?":
				_ = stream.Send(&bds.SendMsgStrResponse{
					Msg: "Gap to Perez in front, 4.4.!",
				})
			case "Hamilton has pitted for inters, for reference, and Perez is three seconds behind. Push, push":
				_ = stream.Send(&bds.SendMsgStrResponse{
					Msg: "I'm not sure it's the right call. It's nearly ready for dry, yeah?",
				})
			case "Charles is coming in for inters. Verstappen 2.8 behind, 2.8.":
				_ = stream.Send(&bds.SendMsgStrResponse{
					Msg: "Happy to stay out.",
				})
			default:
				_ = stream.Send(&bds.SendMsgStrResponse{
					Msg: "Copy, stay out.",
				})
			}
		}
		r.calls = r.calls[:0]
	}
}
