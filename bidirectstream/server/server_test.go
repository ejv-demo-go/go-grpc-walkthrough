package main

import (
	"bytes"
	"context"
	"errors"
	"io"
	"log"
	"net"
	"testing"

	bds "gitlab.com/ejv-demo-go/go-grpc-walkthrough/bidirectstream/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/test/bufconn"
)

func server(ctx context.Context) (client bds.RaceCommsClient, closer func()) {
	buffer := 101024 * 1024
	lis := bufconn.Listen(buffer)

	baseServer := grpc.NewServer()
	bds.RegisterRaceCommsServer(baseServer, &raceCommsServer{})
	go func() {
		if err := baseServer.Serve(lis); err != nil {
			log.Printf("error serving server: %v", err)
		}
	}()

	conn, err := grpc.DialContext(ctx, "",
		grpc.WithContextDialer(func(context.Context, string) (net.Conn, error) {
			return lis.Dial()
		}), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Printf("error connecting to server: %v", err)
	}

	closer = func() {
		err := lis.Close()
		if err != nil {
			log.Printf("error closing listener: %v", err)
		}
		baseServer.Stop()
	}

	client = bds.NewRaceCommsClient(conn)

	return client, closer
}

func TestRaceServerSendMsgBytes(t *testing.T) {
	ctx := context.Background()
	client, closer := server(ctx)
	defer closer()

	type expectation struct {
		out []*bds.SendMsgBytesResponse
		err error
	}

	tests := map[string]struct {
		in       []*bds.SendMsgBytesRequest
		expected expectation
	}{
		"Must_Success": {
			in: []*bds.SendMsgBytesRequest{
				{
					Msg: []byte("Safety Car window is open, just info!"),
				},
				{
					Msg: []byte("Let me know track condition"),
				},
				{
					Msg: []byte("Gap?"),
				},
				{
					Msg: []byte("end"),
				},
			},
			expected: expectation{
				out: []*bds.SendMsgBytesResponse{
					{
						Msg: []byte("copy!"),
					},
					{
						Msg: []byte("Drying up quickly"),
					},
					{
						Msg: []byte("Gap to Sainz behind, Sainz lap time 32.6"),
					},
					{
						Msg: []byte("That's P1!, Great Driving!"),
					},
				},
				err: nil,
			},
		},
	}

	for scenario, tt := range tests {
		t.Run(scenario, func(t *testing.T) {
			outClient, err := client.SendMsgBytes(ctx)

			for _, v := range tt.in {
				if err := outClient.Send(v); err != nil {
					t.Errorf("Err -> %q", err)
				}
			}

			if err := outClient.CloseSend(); err != nil {
				t.Errorf("Err -> %q", err)
			}

			var outs []*bds.SendMsgBytesResponse
			for {
				o, err := outClient.Recv()
				if errors.Is(err, io.EOF) {
					break
				}
				outs = append(outs, o)
			}

			if err != nil {
				if tt.expected.err.Error() != err.Error() {
					t.Errorf("Err -> \nWant: %q\nGot: %q\n", tt.expected.err, err)
				}
			} else {
				if len(outs) != len(tt.expected.out) {
					t.Errorf("Out -> \nWant: %q\nGot : %q", tt.expected.out, outs)
				} else {
					for i, o := range outs {
						if !bytes.Equal(o.Msg, tt.expected.out[i].Msg) {
							t.Errorf("Out -> \nWant: %q\nGot : %q", tt.expected.out, outs)
						}
					}
				}
			}
		})
	}
}

func TestRaceServerSendMsgStr(t *testing.T) {
	ctx := context.Background()
	client, closer := server(ctx)
	defer closer()

	type expectation struct {
		out []*bds.SendMsgStrResponse
		err error
	}

	tests := map[string]struct {
		in       []*bds.SendMsgStrRequest
		expected expectation
	}{
		"Must_Success": {
			in: []*bds.SendMsgStrRequest{
				{
					Msg: "Box this lap, box, pit confirm",
				},
				{
					Msg: "What's the gap?",
				},
				{
					Msg: "Hamilton has pitted for inters, for reference, and Perez is three seconds behind. Push, push",
				},
				{
					Msg: "Charles is coming in for inters. Verstappen 2.8 behind, 2.8.",
				},
				{
					Msg: "Ok, let's hold to see what will happen. Have a good one!",
				},
			},
			expected: expectation{
				out: []*bds.SendMsgStrResponse{
					{
						Msg: "Confirm, Box this lap",
					},
					{
						Msg: "Gap to Perez in front, 4.4.!",
					},
					{
						Msg: "I'm not sure it's the right call. It's nearly ready for dry, yeah?",
					},
					{
						Msg: "Happy to stay out.",
					},
					{
						Msg: "Copy, stay out.",
					},
				},
				err: nil,
			},
		},
	}

	for scenario, tt := range tests {
		t.Run(scenario, func(t *testing.T) {
			outClient, err := client.SendMsgStr(ctx)

			for _, v := range tt.in {
				if err := outClient.Send(v); err != nil {
					t.Errorf("Err -> %q", err)
				}
			}

			if err := outClient.CloseSend(); err != nil {
				t.Errorf("Err -> %q", err)
			}

			var outs []*bds.SendMsgStrResponse
			for {
				o, err := outClient.Recv()
				if errors.Is(err, io.EOF) {
					break
				}
				outs = append(outs, o)
			}

			if err != nil {
				if tt.expected.err.Error() != err.Error() {
					t.Errorf("Err -> \nWant: %q\nGot: %q\n", tt.expected.err, err)
				}
			} else {
				if len(outs) != len(tt.expected.out) {
					t.Errorf("Out -> \nWant: %q\nGot : %q", tt.expected.out, outs)
				} else {
					for i, o := range outs {
						if o.Msg != tt.expected.out[i].Msg {
							t.Errorf("Out -> \nWant: %q\nGot : %q", tt.expected.out, outs)
						}
					}
				}
			}
		})
	}
}
