package main

import (
	"context"
	"errors"
	"log"
	"testing"

	unary "gitlab.com/ejv-demo-go/go-grpc-walkthrough/unary/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/test/bufconn"
)

func server(ctx context.Context) (client unary.RaceLineUpClient, closer func()) {
	buffer := 101024 * 101024
	lis := bufconn.Listen(buffer)

	baseServer := grpc.NewServer()
	unary.RegisterRaceLineUpServer(baseServer, &raceLineUpServer{})
	go func() {
		if err := baseServer.Serve(lis); err != nil {
			log.Printf("error serving server: %v", err)
		}
	}()

	cc, err := grpc.Dial("localhost:50055", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Printf("error connecting to server: %v", err)
	}

	closer = func() {
		err = lis.Close()
		if err != nil {
			log.Printf("error closing listener: %v", err)
		}
		baseServer.Stop()
	}

	client = unary.NewRaceLineUpClient(cc)

	return
}

func TestServerGetDriverName(t *testing.T) {
	ctx := context.Background()
	client, closer := server(ctx)
	defer closer()

	type expectation struct {
		out *unary.GetDriverNameResponse
		err error
	}

	tests := map[string]struct {
		in       *unary.GetDriverNameRequest
		expected expectation
	}{
		"MustSuccess": {
			in: &unary.GetDriverNameRequest{
				Carnumber: "63",
			},
			expected: expectation{
				out: &unary.GetDriverNameResponse{
					Firstname: "George",
					Lastname:  "Russell",
				},
				err: nil,
			},
		},
		"NotFoundNumber": {
			in: &unary.GetDriverNameRequest{
				Carnumber: "88",
			},
			expected: expectation{
				out: &unary.GetDriverNameResponse{},
				err: errors.New("rpc error: code = Unknown desc = no driver found"),
			},
		},
		"InvalidNumber": {
			in: &unary.GetDriverNameRequest{
				Carnumber: "",
			},
			expected: expectation{
				out: &unary.GetDriverNameResponse{},
				err: errors.New("rpc error: code = Unknown desc = invalid number"),
			},
		},
	}

	for scenario, tt := range tests {
		t.Run(scenario, func(t *testing.T) {
			out, err := client.GetDriverName(ctx, tt.in)
			if err != nil {
				if tt.expected.err.Error() != err.Error() {
					t.Errorf("Err -> \nWant: %q\nGot: %q\n", tt.expected.err, err)
				}
			} else {
				if tt.expected.out.Firstname != out.Firstname || tt.expected.out.Lastname != out.Lastname {
					t.Errorf("Out -> \nWant: %q\nGot : %q", tt.expected.out, out)
				}
			}
		})
	}
}

func TestServerGetDriverNum(t *testing.T) {
	ctx := context.Background()
	client, closer := server(ctx)
	defer closer()

	type expectation struct {
		out *unary.GetDriverNumResponse
		err error
	}

	tests := map[string]struct {
		in       *unary.GetDriverNumRequest
		expected expectation
	}{
		"MustSuccess": {
			in: &unary.GetDriverNumRequest{
				Firstname: "Fernando",
				Lastname:  "Alonso",
			},
			expected: expectation{
				out: &unary.GetDriverNumResponse{
					Num:    14,
					Result: "Driver is Fernando Alonso",
				},
				err: nil,
			},
		},
		"NotFoundNumber": {
			in: &unary.GetDriverNumRequest{
				Firstname: "Jensen",
				Lastname:  "Button",
			},
			expected: expectation{
				out: &unary.GetDriverNumResponse{},
				err: errors.New("rpc error: code = Unknown desc = no driver found"),
			},
		},
		"InvalidNameInput": {
			in: &unary.GetDriverNumRequest{
				Firstname: "",
				Lastname:  "",
			},
			expected: expectation{
				out: &unary.GetDriverNumResponse{},
				err: errors.New("rpc error: code = Unknown desc = invalid firstname and lastname input"),
			},
		},
	}

	for scenario, tt := range tests {
		t.Run(scenario, func(t *testing.T) {
			out, err := client.GetDriverNum(ctx, tt.in)
			if err != nil {
				if tt.expected.err.Error() != err.Error() {
					t.Errorf("Err -> \nWant: %q\nGot: %q\n", tt.expected.err, err)
				}
			} else {
				if tt.expected.out.Num != out.Num || tt.expected.out.Result != out.Result {
					t.Errorf("Out -> \nWant: %q\nGot : %q", tt.expected.out, out)
				}
			}
		})
	}
}
