package main

import (
	"context"
	"errors"
	"log"
	// "math/rand"
	"net"
	"net/http"
	_ "net/http/pprof"
	"runtime"
	"strconv"

	"cloud.google.com/go/profiler"
	"gitlab.com/ejv-demo-go/go-grpc-walkthrough/unary/config"
	unary "gitlab.com/ejv-demo-go/go-grpc-walkthrough/unary/proto"
	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

type raceLineUpServer struct {
	unary.RaceLineUpServer
}

func main() {
	cfg := profiler.Config{
		Service:        "raceserver",
		ServiceVersion: "1.0.0",
		// ProjectID must be set if not running on GCP.
		ProjectID: "dev-life-365401",
		DebugLogging: true,
		MutexProfiling: true,

		// For OpenCensus users:
		// To see Profiler agent spans in APM backend,
		// set EnableOCTelemetry to true
		// EnableOCTelemetry: true,
	}

	// Profiler initialization, best done as early as possible.
	if err := profiler.Start(cfg); err != nil {
		log.Fatal(err)
		return
	}

	runtime.SetBlockProfileRate(1)

	tp := config.Init()
	defer func() {
		if err := tp.Shutdown(context.Background()); err != nil {
			log.Printf("Error shutting down tracer provider: %v", err)
		}
	}()

	listener, err := net.Listen("tcp", "0.0.0.0:50055")
	if err != nil {
		_ = errors.New("failed to listen: the port")
	}
	go func() {
		http.ListenAndServe(":8024", nil)
	}()
	log.Print("Server started")
	s := grpc.NewServer(
		grpc.UnaryInterceptor(otelgrpc.UnaryServerInterceptor()),
	)
	unary.RegisterRaceLineUpServer(s, &raceLineUpServer{})

	// Register reflection service on gRPC server.
	reflection.Register(s)

	if err = s.Serve(listener); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}

func (r *raceLineUpServer) GetDriverName(ctx context.Context, req *unary.GetDriverNameRequest) (resp *unary.GetDriverNameResponse, err error) {
	reqNum, _ := strconv.ParseUint(req.Carnumber, 10, 64)

	if len(req.Carnumber) > 3 {
		return &unary.GetDriverNameResponse{}, errors.New("invalid number")
	}
	var drivers = make([]*unary.Driver,30)
	drivers = r.loadDrivers()
	for i := 0; i < len(drivers); i++ {
		if reqNum == drivers[i].Carnumber && drivers[i] != nil {
			resp = &unary.GetDriverNameResponse{
				Firstname: drivers[i].Firstname,
				Lastname:  drivers[i].Lastname,
			}
			return resp, nil
		}
	}

	return resp, errors.New("no driver found")
}

func (r *raceLineUpServer) GetDriverNum(ctx context.Context, req *unary.GetDriverNumRequest) (resp *unary.GetDriverNumResponse, err error) {
	if req.Firstname == "" || req.Lastname == "" {
		return &unary.GetDriverNumResponse{}, errors.New("invalid firstname and lastname input")
	}
	var drivers = make([]*unary.Driver,30)
	drivers = r.loadDrivers()
	for i := 0; i < len(drivers); i++ {
		if req.Firstname == drivers[i].Firstname && req.Lastname == drivers[i].Lastname {
			// randNum := rand.Intn(40)+1
			// SimulateBug(randNum)
			resp = &unary.GetDriverNumResponse{
				Num:    drivers[i].Carnumber,
				Result: "Driver is " + drivers[i].Firstname + " " + drivers[i].Lastname,
			}
			return resp, nil
		}
	}

	return resp, errors.New("no driver found")
}

func (r *raceLineUpServer) ListDrivers(context.Context, *unary.ListDriversRequest) (resp *unary.ListDriversResponse, err error) {
	var drivers = make([]*unary.Driver,30)
	drivers = r.loadDrivers()

	return &unary.ListDriversResponse{

		Driver: drivers,
		Sum:     int32(len(drivers)),
	}, nil
}

// loadDrivers can be defined by yourself or read from database.
func (r *raceLineUpServer) loadDrivers() (drivers []*unary.Driver) {
	drivers = []*unary.Driver{
		0: {
			Firstname: "Max",
			Lastname:  "Verstappen",
			Carnumber:    1,
			Constructor:  "Red Bull Racing",
		},
		1: {
			Firstname: "Sergio",
			Lastname:  "Perez",
			Carnumber:    11,
			Constructor:  "Red Bull Racing",
		},
		2: {
			Firstname: "Lewis",
			Lastname:  "Hamilton",
			Carnumber:    44,
			Constructor:  "Mercedes",
		},
		3: {
			Firstname: "Fernando",
			Lastname:  "Alonso",
			Carnumber:    14,
			Constructor:  "Aston Martin",
		},
		4: {
			Firstname: "Carlos",
			Lastname:  "Sainz",
			Carnumber:    55,
			Constructor:  "Ferrari",
		},
		5: {
			Firstname: "Lando",
			Lastname:  "Norris",
			Carnumber:    4,
			Constructor:  "McLaren",
		},
		6: {
			Firstname: "Charles",
			Lastname:  "LeClerc",
			Carnumber:    16,
			Constructor:  "Ferrari",
		},
		7: {
			Firstname: "George",
			Lastname:  "Russell",
			Carnumber:    63,
			Constructor:  "Mercedes",
		},
		8: {
			Firstname: "Oscar",
			Lastname:  "Piastri",
			Carnumber:    81,
			Constructor:  "McLaren",
		},
		9: {
			Firstname: "Pierre",
			Lastname:  "Gasly",
			Carnumber:    10,
			Constructor:  "Alpine",
		},
		10: {
			Firstname: "Lance",
			Lastname:  "Stroll",
			Carnumber:    18,
			Constructor:  "Aston Martin",
		},
		11: {
			Firstname: "Yuki",
			Lastname:  "Tsunoda",
			Carnumber:    22,
			Constructor:  "AlphaTauri",
		},
		12: {
			Firstname: "Guanyu",
			Lastname:  "Zhou",
			Carnumber:    24,
			Constructor:  "Alfa Romeo",
		},
		13: {
			Firstname: "Esteban",
			Lastname:  "Ocon",
			Carnumber:    31,
			Constructor:  "Alpine",
		},
		14: {
			Firstname: "Alexander",
			Lastname:  "Albon",
			Carnumber: 23,
			Constructor: "Williams",
		},
		15: {
			Firstname: "Valtteri",
			Lastname: "Botas",
			Carnumber: 77,
			Constructor: "Alfa Romeo",
		},
		16: {
			Firstname: "Nico",
			Lastname: "Hulkenberg",
			Carnumber: 27,
			Constructor: "Haas F1 Team",
		},
		17: {
			Firstname: "Kevin",
			Lastname: "Magnussen",
			Carnumber: 20,
			Constructor: "Haas F1 Team",
		},
		18: {
			Firstname: "Logan",
			Lastname: "Sargent",
			Carnumber: 2,
			Constructor: "Williams",
		},
		19: {
			Firstname: "Daniel",
			Lastname: "Ricciardo",
			Carnumber: 3,
			Constructor: "AlphaTauri",
		},
		20: {
			Firstname: "Liam",
			Lastname: "Lawson",
			Carnumber: 40,
			Constructor: "AlphaTauri",
		},
		21: {
			Firstname: "Nyck",
			Lastname: "De Vries",
			Carnumber: 21,
			Constructor: "AlphaTauri",
		},
	}
	return
}

// func SimulateBug(n int) int {
//     if n <= 1 {
// 		return n
// 	}
// 	return SimulateBug(n-1) + SimulateBug(n-2)
// }