package main

import (
	"context"
	"log"
	_ "net/http/pprof"
	"runtime"
	"strconv"

	"time"

	"cloud.google.com/go/profiler"
	"gitlab.com/ejv-demo-go/go-grpc-walkthrough/unary/config"
	unary "gitlab.com/ejv-demo-go/go-grpc-walkthrough/unary/proto"
	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/metadata"
)

func main() {
	cfg := profiler.Config{
		Service:        "raceclient",
		ServiceVersion: "1.0.0",
		// ProjectID must be set if not running on GCP.
		ProjectID:      "dev-life-365401",
		DebugLogging:   true,
		MutexProfiling: true,

		// For OpenCensus users:
		// To see Profiler agent spans in APM backend,
		// set EnableOCTelemetry to true
		// EnableOCTelemetry: true,
	}
	// Profiler initialization, best done as early as possible.
	if err := profiler.Start(cfg); err != nil {
		log.Fatal(err)
		return
	}

	runtime.SetBlockProfileRate(1)

	tp := config.Init()
	defer func() {
		if err := tp.Shutdown(context.Background()); err != nil {
			log.Printf("Error shutting down tracer provider: %v", err)
		}
	}()

	cc, err := grpc.Dial("localhost:50055", grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithUnaryInterceptor(otelgrpc.UnaryClientInterceptor()),
	)
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	defer func(cc *grpc.ClientConn) {
		err = cc.Close()
		if err != nil {
			log.Println(err)
		}
	}(cc)

	client := unary.NewRaceLineUpClient(cc)

	log.Println("Starting to do a Unary Phone RPC...")
	for {

		doUnary(client)
	}
}

func doUnary(client unary.RaceLineUpClient) {
	GetServerDriverName(client)
	GetServerDriverNum(client)
	ListDrivers(client)
}

func GetServerDriverName(client unary.RaceLineUpClient) {
	req := &unary.GetDriverNameRequest{
		Carnumber: "81",
	}
	md := metadata.Pairs(
		"timestamp", time.Now().Format(time.StampNano),
		"request", req.Carnumber,
	)
	ctx := metadata.NewOutgoingContext(context.Background(), md)
	res, err := client.GetDriverName(ctx, req)
	if err != nil {
		log.Fatalf("error while calling Unary Phone RPC: %v", err)
	}

	log.Println("Response from GetDriverName: ", res.Firstname+" "+res.Lastname)
}

func GetServerDriverNum(client unary.RaceLineUpClient) {
	req := &unary.GetDriverNumRequest{
		Firstname: "Oscar",
		Lastname:  "Piastri",
	}
	md := metadata.Pairs(
		"timestamp", time.Now().Format(time.StampNano),
		"request", req.Firstname+" "+req.Lastname,
	)
	ctx := metadata.NewOutgoingContext(context.Background(), md)
	res, err := client.GetDriverNum(ctx, req)
	if err != nil {
		log.Fatalf("error while calling Unary Phone RPC: %v", err)
	}

	log.Println("Response from GetDriverNum: ", strconv.FormatUint(res.Num, 10)+" "+res.Result)
}

func ListDrivers(client unary.RaceLineUpClient) {
	md := metadata.Pairs(
		"timestamp", time.Now().Format(time.StampNano),
		"req", "ListDrivers",
	)
	ctx := metadata.NewOutgoingContext(context.Background(), md)
	res, err := client.ListDrivers(ctx, &unary.ListDriversRequest{})
	if err != nil {
		log.Fatalf("error while calling Unary Phone RPC: %v", err)
	}
	log.Println("Response from ListDrivers: sum is ", res.Sum)
	log.Println("drivers list is ", res.Driver)
}
