package config

import (
	"context"
	"log"
	"os"

	texporter "github.com/GoogleCloudPlatform/opentelemetry-operations-go/exporter/trace"
	"go.opentelemetry.io/contrib/detectors/gcp"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"

	//"go.opentelemetry.io/otel/exporters/otlp/otlptrace"
	//"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracegrpc"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	//"google.golang.org/api/option"
	//"google.golang.org/grpc"

	//"google.golang.org/grpc/credentials"
	//"google.golang.org/grpc/credentials/google"
	//"google.golang.org/grpc/credentials/insecure"
)

// Init configures an OpenTelemetry exporter and trace provider
func Init() *sdktrace.TracerProvider {
	var (
		serviceName = os.Getenv("OTEL_SERVICE_NAME")
		projectID   = os.Getenv("GOOGLE_CLOUD_PROJECT")
		
	)

	

	exporter, err := texporter.New(
		texporter.WithProjectID(projectID),

	)

	if err != nil {
		log.Fatal(err)
	}


	resources, err := resource.New(
		context.Background(),
		resource.WithAttributes(
			attribute.String("service.name", serviceName),
		),
		resource.WithDetectors(gcp.NewDetector()),
	)
	if err != nil {
		log.Printf("Could not set resources: %v", err)
	}

	traceProvider := sdktrace.NewTracerProvider(
		sdktrace.WithSampler(sdktrace.AlwaysSample()),
		sdktrace.WithBatcher(exporter),
		sdktrace.WithResource(resources),
	)

	otel.SetTracerProvider(traceProvider)
	otel.SetTextMapPropagator(propagation.NewCompositeTextMapPropagator(propagation.TraceContext{}, propagation.Baggage{}))

	return traceProvider
}
